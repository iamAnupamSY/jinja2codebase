# import all the required modules
from jinja2 import Environment, FileSystemLoader
from weasyprint import HTML
import os

# variables initialization
TEMPLATE_PATH = os.path.dirname(os.path.abspath(__file__))
#env = Environment(loader=FileSystemLoader("."))

env = Environment(loader=FileSystemLoader(TEMPLATE_PATH))
template = env.get_template("infoTemplate.html")

template_vars = {"title" : "Employee INFO",
                 "full_name" : "Anupam Yadav",
                 "company_name" : "ABC Solutions",
                 "contact_no" : 1234567890,
                 }

html_output = template.render(template_vars)

with open("info.txt", "w") as fobj:
    fobj.write(html_output)


HTML(string=html_output).write_pdf("report.pdf")
